from typing import List, Dict


class TreeStore:
    def __init__(self, items):
        self.items: List[Dict] = items

    def getAll(self):
        return self.items

    # implemented by the search method
    def getItem(self, pk):
        result = [result for result in self.items if result.get('id') == pk]
        if len(result) < 1:
            raise ValueError(f"Объект Item с id: {pk} не найден")
        elif len(result) > 1:
            raise ValueError(f"Объект Item с id: {pk} возвращено несколько объектов")
        return result[0]

    # implemented by the sampling method
    # def getItem(self, pk: int):
    #     pk -= 1
    #     items_count = len(self.items)
    #     if pk > items_count or 0 >= pk:
    #         raise ValueError(f"Объект Item с id: {pk} не найден")
    #     return self.items[pk]

    def getChildren(self, pk):
        return [result for result in self.items if result.get('parent') == pk]

    def getAllParents(self, pk):
        result = []
        while True:
            pk -= 1
            result.append(self.items[pk])
            pk = self.items[pk].get("parent")
            if isinstance(pk, str):
                break
        return result[1:]


items = [
    {"id": 1, "parent": "root"},
    {"id": 2, "parent": 1, "type": "test"},
    {"id": 3, "parent": 1, "type": "test"},
    {"id": 4, "parent": 2, "type": "test"},
    {"id": 5, "parent": 2, "type": "test"},
    {"id": 6, "parent": 2, "type": "test"},
    {"id": 7, "parent": 4, "type": None},
    {"id": 8, "parent": 4, "type": None}
]

if __name__ == '__main__':
    ts = TreeStore(items)
    print(f"result getAll method: {ts.getAll()}")
    print(f"result getItem method: {ts.getItem(7)}")
    print(f"result getChildren method: {ts.getChildren(4)}")
    print(f"result getChildren method: {ts.getChildren(5)}")
    print(f"result getAllParents method: {ts.getAllParents(7)}")
